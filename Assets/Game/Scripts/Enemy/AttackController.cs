using UnityEngine;
using UnityEngine.Analytics;

namespace Game.Scripts.Enemy
{
    public class AttackController : MonoBehaviour
    {
        private Enemy _enemy;
        private Transform _player;
        private Transform _transform;

        public bool AutoAttack;
        
        private void Awake()
        {
            _enemy = GetComponent<Enemy>();
        }

        private void Start()
        {
            _player = GameObject.FindWithTag("Player").transform;
            _transform = transform;
            AutoAttack = true;
        }
        
        private void Update()
        {
            if (AutoAttack)
            {
                var distance = Vector3.Distance(_transform.position, _player.position);
                if (distance < _enemy.RangeOfAttack)
                {
                    _enemy.SetNavmeshStoping(true);
                    _enemy.Attack();
                }
                else
                {
                    _enemy.SetNavmeshStoping(false);
                } 
            }
        }
    }
}