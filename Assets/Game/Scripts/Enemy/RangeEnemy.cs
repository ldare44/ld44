using UnityEngine;

namespace Game.Scripts.Enemy
{
    public class RangeEnemy : Enemy
    {
        public GameObject Projectile;
        public Transform SpawnPoint;

        public float BulletSpeed = 10f;
        
        protected override void Start()
        {
            base.Start();
            RangeOfAttack = 8f;
        }

        protected override void PerformAttack()
        {
            base.PerformAttack();
            if (_navMeshAgent.enabled)
                _navMeshAgent.isStopped = false;
            _animator.SetTrigger("Attack");
            SpawnProjectile();
            _audioSource.PlayOneShot(EnemyClips[(int)EnemyState.Attack]);

            //hpControl.RecieveDamage(damage);
            DidAttack();
        }

        private void SpawnProjectile()
        {
            var projectile = Instantiate(Projectile, SpawnPoint.position,  SpawnPoint.rotation).GetComponent<Projectile>();
            //projectile.transform.Rotate(90, projectile.transform.rotation.y, projectile.transform.rotation.z);
            var rbProjectile = projectile.GetComponent<Rigidbody>();
            projectile.Damage = Damage;
            if (_isSlave)
            {
                rbProjectile.AddForce(rbProjectile.transform.forward * BulletSpeed, ForceMode.Impulse);
            }
            else
            {
                rbProjectile.transform.LookAt(Controller.transform);
                rbProjectile.AddForce(rbProjectile.transform.forward * BulletSpeed, ForceMode.Impulse);
            }
        }
        
        protected override void Update()
        {
            base.Update();
            if (_navMeshAgent.enabled) _animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);
        }
    }
}