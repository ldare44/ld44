﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Enemy
{
     
    public class EnemyHealthControl : MonoBehaviour
    {
        public Canvas canvas;
        public Image foreGroundImage;

        private Enemy _enemy;
        // Start is called before the first frame update
        void Awake()
        {
            _enemy = GetComponentInParent<Enemy>();
        }

        // Update is called once per frame
        void Update()
        {
            foreGroundImage.fillAmount = _enemy.Health / _enemy.MaxHealth;
            canvas.transform.LookAt(Camera.main.transform);
        }
    }
}
