﻿using System.Collections;
using System.Collections.Generic;
using Game.Scripts.Enemy;
using Game.Scripts.HpManager;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    public float Damage;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnCollisionEnter(Collision other)
    {
        /*
        if (other.transform.CompareTag("Player"))
        {
            var hpControl = other.transform.GetComponentInParent<VenomController>();
            hpControl.RecieveDamage(Damage);
        }
        */
        
        if (other.transform.CompareTag("Enemy"))
        {
            Debug.Log("Hit enemy " + other.transform.name);
            var enemy = other.transform.GetComponentInParent<Enemy>();
            if (!enemy.IsSlave)
            {                
                enemy.UpdateHp(enemy.Health - Damage);
                _audioSource.Play();
            } 
        }
    }
}
