using System;
using System.Collections.Generic;
using Game.Scripts.HpManager;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Scripts.Enemy
{
    [Serializable]
    public class EnemySuper
    {
        public float MoveSpeed;
        public float Damage;
        public float AttackCooldown;
    }
    
    public class Enemy : MonoBehaviour
    {
        public event Action DeathEvent;
        
        public enum EnemyState
        {
            Idle,
            Walk,
            Attack,
            Death
        }

        public Transform Target;
        public VenomController Controller;

        public GameObject DeathParticles;
        public Transform DeathParticleTransform;
        
        public float Health = 100f;
        public float MaxHealth = 100f;
        public float Damage = 5f;
        public float RangeOfAttack = 1f;
        public float AttackCooldown = 1f;
        public float PlayerAttackCooldown = 1f;
        public float PlayerDamage = 1f;
        public float CocoonTimeout = 3f;
        public bool IsSuperSoldier;

        public float VisionRange = 20.0f;
        
        public List<AudioClip> EnemyClips;
        public GameObject Cocoon;
        public GameObject Venom;
        public List<GameObject> EnemyBodyParts;

        public List<EnemySuper> EnemySupers;
        
        protected Animator _animator;
        protected float _attackTimeout = 0f;
        protected AudioSource _audioSource;
        protected NavMeshAgent _navMeshAgent;
        protected bool _isSlave;

        public bool IsSlave => _isSlave;

        private bool _isAttacking;

        public bool IsAttacking => _isAttacking;

        protected Transform _transform;
        private float _cocoonTimer;
        private float _oldDamage;
        private bool _isCocoon;

        private Renderer[] _renderers;
        private List<Material> _oldColors = new List<Material>();

        private AttackController _attackController;

        protected virtual void Awake()
        {
            _attackController = GetComponent<AttackController>();
            _audioSource = GetComponent<AudioSource>();
            _animator = GetComponent<Animator>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }

        protected virtual void Start()
        {
            _renderers = GetComponentsInChildren<Renderer>();
            for (int i = 0; i < _renderers.Length; i++)
            {
                var mat = new Material(new Material(_renderers[i].material));
                //mat.SetFloat("");
                //Debug.Log("mat metallic = " + mat.GetFloat("_Metallic"));
                //Debug.Log("mat glossines = " + mat.GetFloat("_Glossiness"));
                
                _oldColors.Add(mat);
            }
            var player = GameObject.FindGameObjectWithTag("Player");
            Target = player.transform;
            Controller = Target.GetComponent<VenomController>();
            _transform = transform;
            Cocoon.SetActive(false);
            Venom.SetActive(false);
        }

        public void Attack()
        {
            if (_isSlave)
            {
                if (_attackTimeout <= 0f && !_isAttacking)
                {
                    PerformAttack();
                }
                return;
            }
            
            if (_attackTimeout <= 0f && IsPlayerInSight() && !_isAttacking)
            {
                PerformAttack();
            }
        }

        public void SetNavmeshStoping(bool isStoped)
        {
            if (_navMeshAgent.enabled)
            _navMeshAgent.isStopped = isStoped;
        }

        protected virtual void PerformAttack()
        {
            _isAttacking = true;
        }

        protected virtual void DidAttack()
        {
            _attackTimeout = _isSlave ? PlayerAttackCooldown : AttackCooldown;
            _isAttacking = false;
        }

        private void FixedUpdate()
        {
           if (_navMeshAgent.enabled && IsPlayerInSight()) MoveTo(Target);
        }

        protected virtual void Update()
        {
            if (!Target)
            {
                Target = GameObject.FindGameObjectWithTag("Player").transform.parent;
                Controller = Target.GetComponent<VenomController>();
            }
            else
            {
                if (!_isSlave) RotateTowards(Target);
            }
            
            if (_attackTimeout > 0)
                _attackTimeout -= Time.deltaTime;

            // Cocoon Timer
            if (_isCocoon)
            {
                if (_cocoonTimer > 0)
                    _cocoonTimer -= Time.deltaTime;
                else
                    BecomeSuperSoldire();
                
            }
        }

        public virtual bool UpdateHp(float hPCount, bool vampiricRemove = false, bool dontMove = false)
        {
            if (_navMeshAgent.enabled && !dontMove)
                _navMeshAgent.velocity -= _transform.forward * 5;
            
            Health = Mathf.Clamp(hPCount, 0 ,MaxHealth);
            if (Health <= float.Epsilon)
            {
               Debug.Log("Death vampiric? = " + vampiricRemove + "; Health = " + Health);
               Death(vampiricRemove);
               return true;
            }

            if (!_isSlave)
            {
                iTween.ColorFrom(gameObject, iTween.Hash(
                    "color", Color.red,
                    "time", 0.2f,
                    "easytype", iTween.EaseType.easeOutQuad
                    //"oncompletetarget", gameObject,
                    //"oncomplete", "ReturnColor"
                ));
            }
            
            return false;
        }
        
        private void ReturnColor()
        {
           /* for (int i = 0; i < _renderers.Length; i++)
            {
                _renderers[i].material.color = _oldColors[i].color;
                _renderers[i].material.SetFloat("_Metallic", _oldColors[i].GetFloat("_Metallic"));
                _renderers[i].material.SetFloat("_Glossiness", _oldColors[i].GetFloat("_Glossiness"));
                Debug.Log("mat Name = " + _renderers[i].material.name);
                Debug.Log("mat become metallic = " + _renderers[i].material.GetFloat("_Metallic"));
                Debug.Log("mat become gloss = " + _renderers[i].material.GetFloat("_Glossiness"));
            }*/
        }

        private void MoveTo(Transform t)
        {
            if (!_navMeshAgent.enabled) return;
            _navMeshAgent.SetDestination(t.position);
        }
        
        private void Death(bool superDeath = false)
        {
            if (superDeath)
            {
               BecomeSuperSoldire(true);
            }
            else
            {
                //_audioSource.PlayOneShot(EnemyClips[(int)EnemyState.Death]);
                var deathParticles = Instantiate(DeathParticles, DeathParticleTransform.position,  DeathParticleTransform.rotation);
                Destroy(deathParticles, 1);
                gameObject.SetActive(false);
                GameSingleton.Instance.UpdateKiilsCount(1);
                DeathEvent?.Invoke();
            }
        }

        public bool IsPlayerInSight()
        {
            RaycastHit hit;
            var rayDirection = Target.position - _transform.position;
            
            if (!Physics.Raycast(_transform.position, rayDirection, out hit, VisionRange))
            {
                return false;
            }
            
            return hit.transform == Target;
        }

        public void SetAnimationSpeed(float speed)
        {
            _animator.SetFloat("Speed", speed);
        }
        
        private void RotateTowards(Transform target)
        {
            var direction = (target.position - _transform.position).normalized;
            var lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));    // flattens the vector3
            _transform.rotation = Quaternion.Slerp(_transform.rotation, lookRotation, Time.deltaTime * _navMeshAgent.angularSpeed/10);
        }
        
        /*
        public Transform IsEnemyInSight()
        {
            RaycastHit hit;
            var rayDirection = _transform.forward;
            
            Debug.DrawLine(_transform.position, rayDirection);

            if (Physics.SphereCast(_transform.position, 3, rayDirection, out hit, 4f))
            {
                if (hit.transform && hit.transform.CompareTag("Enemy"))
                {
                    return hit.transform;
                }
            }
            
            return null;
        }
        */

        public void BecomeSlave(Transform parentTransform)
        {
            _navMeshAgent.enabled = false;
            Venom.SetActive(true);
            parentTransform.position = new Vector3
            (
                _transform.position.x, 
                parentTransform.position.y, 
                _transform.position.z
            );
            _oldDamage = Damage;
            Damage = PlayerDamage;
            _transform.rotation = parentTransform.rotation;
            _attackController.AutoAttack = false;
            _transform.parent = parentTransform;
            _isSlave = true;
        }

        public void FreeFromSlavery(bool super = false)
        {
            foreach (var part in EnemyBodyParts)
            {
                part.SetActive(false);
            }
            Venom.SetActive(false);

            Cocoon.SetActive(true);
            _transform.parent = null;
            _isSlave = false;
            _cocoonTimer = CocoonTimeout;
            _isCocoon = true;
            Debug.Log("free from slavery");
        }

        public void BecomeSuperSoldire(bool uber = false)
        {
            if (uber)
            {
                _transform.parent = null;
                _isSlave = false; // hack govnokod
                Venom.SetActive(false);
            }

            // super levels
            EnemySuper enemySuper;
            if (Health <= MaxHealth * 0.2)
                enemySuper = EnemySupers[2];
            else if (Health <= MaxHealth * 0.5)
                enemySuper = EnemySupers[1];
            else
                enemySuper = EnemySupers[0];
            Damage = enemySuper.Damage;
            _navMeshAgent.speed = enemySuper.MoveSpeed;
            AttackCooldown = enemySuper.AttackCooldown;
            Damage = _oldDamage;
            Health = MaxHealth/2;

            Debug.Log("become super soldier Health = " + Health);
            _attackController.AutoAttack = true;
            _isCocoon = false;
            IsSuperSoldier = true;
            _navMeshAgent.enabled = true;
            Cocoon.SetActive(false);

            foreach (var part in EnemyBodyParts)
            {
                part.SetActive(true);
            }
            _transform.localScale = Vector3.one;
        }
    }
}