﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Game.Scripts.Enemy;
using Game.Scripts.HpManager;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Damage = 5;
    public MeshRenderer[] Meshes;
    private AudioSource _audioSource;
    private SphereCollider _collider;
    
    private void Awake()
    {
        //_renderers = GetComponents<MeshRenderer>();
        _collider = GetComponent<SphereCollider>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            var hpControl = other.transform.GetComponentInParent<VenomController>();
            hpControl.RecieveDamage(Damage);
        }

        if (other.transform.CompareTag("Enemy"))
        {
            //Debug.Log("Hit enemy");
            var enemy = other.transform.GetComponentInParent<Enemy>();
            enemy.UpdateHp(enemy.Health - Damage, false, true);
        }

        foreach (var render in Meshes)
        {
            render.enabled = false;
        }
        
        _collider.enabled = false;
        
        _audioSource.Play();
        Destroy(gameObject, _audioSource.clip.length + 0.2f);
    }
}
