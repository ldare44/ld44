using System;
using UnityEngine;

namespace Game.Scripts.Enemy
{
    public class MeleeEnemy : Enemy
    {
        public GameObject MeleeObject;
        
        protected override void PerformAttack()
        {
            base.PerformAttack();
            _animator.SetTrigger("Attack");
        }

        protected override void Start()
        {
            base.Start();
            MeleeObject.GetComponent<MeleeAttack>().Damage = Damage;
            MeleeObject.SetActive(false);
        }

        protected override void DidAttack()
        {
            base.DidAttack();
            if (_navMeshAgent.enabled)
                _navMeshAgent.isStopped = false;

            if (_isSlave)
            {
                MeleeObject.SetActive(true);
               /* var enemyInSight = IsEnemyInSight();
                if (enemyInSight)
                {
                    Debug.Log("Slave try to attack: enemyInSight" + enemyInSight.name);
                    var enemy = enemyInSight.GetComponentInParent<Enemy>();
                    enemy.UpdateHp(enemy.Health - Damage);
                }*/
                    
            }
            else
            {
                Controller.RecieveDamage(Damage);
            }
            
            _audioSource.PlayOneShot(EnemyClips[(int)EnemyState.Attack]);
        }

        protected override void Update()
        {
            base.Update();
            if (_navMeshAgent.enabled) _animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);
        }

        protected void EndPlayerAttack()
        {
            if (_isSlave) MeleeObject.SetActive(false);
        }
    }
}