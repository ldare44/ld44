﻿using System.Collections;
using System.Collections.Generic;
using Game.Scripts.Player;
using UnityEngine;
using VenomController = Game.Scripts.HpManager.VenomController;

public class IsometricMovementController : MonoBehaviour
{
    public Transform Target;

    public Camera CameraObject;

    public bool UseGamepad;
    public LayerMask MouseLayerMask;
    
    private Vector3 _forward, _right;
    public float MoveSpeed = 40f;
    public float RotationSpeed = 40f;
    public float DistanceToTarget;
    
    private CharacterController _characterController;
    private VenomController _venomController;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        _characterController = Target.GetComponent<CharacterController>();
        _venomController = Target.GetComponent<VenomController>();
        
        _forward = CameraObject.transform.forward;
        _forward.y = 0;
        _forward = Vector3.Normalize(_forward);
        _right = Quaternion.Euler(new Vector3(0, 90, 0)) * _forward;
        
        CameraObject.transform.rotation = Quaternion.Euler (30, 45, 0);
        CameraObject.transform.position = Target.position - (Quaternion.Euler (30, 45, 0) * Vector3.forward * 5);
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.anyKey)
        Move();
    }

    private void Move()
    {
        var input = GetInput();
        var rInput = GetInput("Right");
        
        RaycastHit hitInfo;
        Physics.SphereCast(_characterController.transform.position, _characterController.radius, Vector3.down, out hitInfo,
            0.5f/*_characterController.height / 2f*/, // 0.5f fix edge bumping (when falling)
            Physics.AllLayers, QueryTriggerInteraction.Ignore);
        
        //if (input.magnitude > 0.0f)
        {
            //var direction = new Vector3(input.x, 0, input.y);
            var rightMovement = _right  * Time.deltaTime * input.x;
            var upMovement = _forward  * Time.deltaTime * input.y;
            
            var heading = Vector3.Normalize(rightMovement + upMovement);
            heading = Vector3.ProjectOnPlane(heading, hitInfo.normal).normalized;

            if (UseGamepad)
            {
                if (rInput == Vector2.zero)
                {
                    Target.transform.forward = heading == Vector3.zero ? Target.transform.forward : heading;
                }
                else
                {
                    rightMovement = _right * RotationSpeed * Time.deltaTime * rInput.x;
                    upMovement = _forward * RotationSpeed * Time.deltaTime * -rInput.y;
                    var rotationHeading = Vector3.Normalize(rightMovement + upMovement);
                
                    Target.transform.forward = rotationHeading;
                }
            }
            else
            {
                Ray cameraRay = CameraObject.ScreenPointToRay(Input.mousePosition);
                RaycastHit raycastHit;

                if (Physics.Raycast(cameraRay, out raycastHit,100f, MouseLayerMask))
                {
                    if (raycastHit.transform != null)
                    {
                        var target = new Vector3(raycastHit.point.x, _characterController.transform.position.y, raycastHit.point.z);
                        Target.transform.LookAt(target);
                    }
                }
            }
            
            if (_characterController.isGrounded)
                heading.y = Physics.gravity.y;
            else
                heading += Physics.gravity * 2;
            
            //Target.transform.position += heading * MoveSpeed * Time.deltaTime;
            _characterController.Move(heading * MoveSpeed * Time.deltaTime);
            
            CameraObject.transform.rotation = Quaternion.Euler (30, 45, 0);
            CameraObject.transform.position = Target.position - (Quaternion.Euler (30, 45, 0) * Vector3.forward * DistanceToTarget);
        }

    }

    private void Rotation(Vector2 moveInput)
    {
        var input = GetInput("Right");
       
        
        if (input == Vector2.zero)
        {
            var rightMovement = _right * RotationSpeed * Time.deltaTime * moveInput.x;
            var upMovement = _forward * RotationSpeed * Time.deltaTime * moveInput.y;
            var heading = Vector3.Normalize(rightMovement + upMovement);
        }
        else
        {
            var rightMovement = _right * RotationSpeed * Time.deltaTime * input.x;
            var upMovement = _forward * RotationSpeed * Time.deltaTime * input.y;
            var heading = Vector3.Normalize(rightMovement + upMovement);
            Target.transform.forward = heading;
            Target.transform.position += heading * MoveSpeed * Time.deltaTime;

        }
    }

    public Vector2 GetInput(string prefix = "", bool debug = false)
    {
        Vector2 input = new Vector2
        {
            x = Input.GetAxis(prefix + "Horizontal"),
            y = Input.GetAxis(prefix + "Vertical")
        };
        if (input.sqrMagnitude > 1) input.Normalize(); // ???? Dafug
        return input;
    }
}