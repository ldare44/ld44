﻿using Game.Scripts.Ui;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSingleton : MonoBehaviour
{
    public static GameSingleton Instance;

    public int KillsCount
    {
        get;
        private set;
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        InitGame();
    }

    // Инициализируем все здесь
    private void InitGame()
    {
    }

    public void Win()
    {
        UiManager.Instance.ActivatePanel();
    }

    public void Death()
    {
        UiManager.Instance.ActivatePanel(true);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartCreditsScene()
    {
        SceneManager.LoadScene("Credits");
    }

    public void UpdateKiilsCount(int count = 0)
    {
        KillsCount += count;
    }
}