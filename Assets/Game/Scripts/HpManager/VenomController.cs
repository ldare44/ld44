using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.HpManager
{
    using Enemy = Enemy.Enemy;

    public class VenomController : MonoBehaviour
    {
        private float _currentHp;

        [SerializeField] private float _speedLosingHp = 1;
        [SerializeField] private float _speedStealingHp = 1;
        [SerializeField] private float _maxHpValue = 10;
        [SerializeField] private float _hostingPrice = 2;
        [SerializeField] private float _radius = 2;

        [SerializeField] private Slider _hpSlider;
        [SerializeField] private Slider _hostHpSlider;
        
        private Animation shakeAnimHost;
        private Animation shakeAnimVenom;

        public LayerMask EnemyLayerMask;
        
        private bool _inHost;
        private bool _vampiric;
        private Transform _host;
        private Enemy _hostEnemy;
        private Transform _transform;

        public GameObject Venom;

        private CharacterController _characterController;
        private Animator _venomAnimator;
        private AudioSource _audioSource;
        
        private void Awake()
        {
            _characterController = GetComponent<CharacterController>();
            _audioSource = GetComponent<AudioSource>();
            _hpSlider.maxValue = _maxHpValue;
            _hpSlider.value = _maxHpValue;
            _currentHp = _maxHpValue;
            _inHost = false;
            _hostHpSlider.gameObject.SetActive(false);

            shakeAnimHost = _hostHpSlider.GetComponent<Animation>();
            shakeAnimVenom = _hpSlider.GetComponent<Animation>();
        }

        private void Start()
        {
            _transform = transform;
            _venomAnimator = Venom.GetComponent<Animator>();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _radius);
        }

#region UpdateRegion

        private void Update()
        {
            if (!_inHost)
            {
                _venomAnimator.SetFloat("Speed", _characterController.velocity.magnitude);
                
                var overlapSphere = Physics.OverlapSphere(transform.position, _radius, EnemyLayerMask);

                shakeAnimVenom.Play();
                if (overlapSphere.Length > 0)
                {
                    var hostCollider = NearestCollider(overlapSphere);
                
                    if (Input.GetButtonDown("Right_Bumper") && hostCollider != null)
                    {
                        shakeAnimVenom.Stop();
                        SlavingHost(hostCollider);
                    }
                }
            }
            else
            {
                _hostEnemy.SetAnimationSpeed(_characterController.velocity.magnitude);
                if (Input.GetAxis("Right_Trigger") > 0.5f || Input.GetMouseButton(0))
                {
                    _hostEnemy.Attack();
                }
                
                if (Input.GetButtonDown("Right_Bumper") && _host)
                {
                    ReleaseHost();
                }

                /*
                if (_hostEnemy.IsAttacking)
                {
                    _characterController.Move(Vector3.zero);
                }
                */
            }
            
            if (_vampiric)
            {
                shakeAnimHost.Play();
            }
            else
            {
                shakeAnimHost.Stop();
            }
            
            UpdateHp();
        }
        
        private void UpdateHp()
        {
            if (!_inHost)
            {
                var hpPoints = Time.deltaTime * _speedLosingHp * -1;
                _currentHp = Mathf.Clamp(_currentHp + hpPoints, 0, _maxHpValue);
                _hostHpSlider.gameObject.SetActive(false);
            }

            if (_inHost && _hostEnemy != null)
            {
                _vampiric = false;
                var hpPoints = Time.deltaTime * _speedLosingHp * -1;
                if ((Input.GetAxis("Left_Trigger") > 0.5f || Input.GetMouseButton(1))
                    && _host != null && _inHost)
                {
                    hpPoints = Time.deltaTime * _speedLosingHp * -1 * _speedStealingHp;
                    _currentHp = Mathf.Clamp(_currentHp + hpPoints * -1, 0, _maxHpValue);
                    _vampiric = true;
                    if (!_audioSource.isPlaying) _audioSource.Play();
                }

                if (!_vampiric && _audioSource.isPlaying)
                {
                    _audioSource.Stop();
                }
                
                _hostHpSlider.value = _hostEnemy.Health;
                if (_hostEnemy.UpdateHp(_hostEnemy.Health + hpPoints, _vampiric))
                {
                    Debug.Log("Release Host");
                    ReleaseHost();
                }
            }
            
            _hpSlider.value = _currentHp;

            if (_currentHp <= 0)
            {
                Death();
            }
        }

        #endregion
        
        private void SlavingHost(Collider hostCollider)
        {
            _host = hostCollider.gameObject.transform.parent;
            _hostEnemy = _host.GetComponent<Enemy>();
            
            if (_hostEnemy.IsSuperSoldier) return;
            
            if (!_inHost)
            {
                _currentHp -= _hostingPrice;
                Venom.SetActive(false);
            }
            
            Debug.Log(_host.name);
            
            _hostEnemy.DeathEvent += Death;
            _hostHpSlider.maxValue = _hostEnemy.MaxHealth;
            _hostHpSlider.value = _hostEnemy.Health;
            _hostHpSlider.gameObject.SetActive(true);
            
            _hostEnemy.BecomeSlave(_transform);
            
            _inHost = !_inHost;
        }

        // Collision
        private Collider NearestCollider(Collider[] colliders)
        {
            var distance = 0f;
            var nearEnemey = colliders.FirstOrDefault();
            foreach (var collider1 in colliders)
            {
                var enemy = collider1.gameObject.transform.parent.GetComponent<Enemy>();
                if (enemy && enemy.IsSuperSoldier) continue;

                var dist = Vector3.Distance(transform.position, collider1.transform.position);

                if (distance >= dist)
                {
                    continue;
                }

                distance = dist;
                nearEnemey = collider1;
            }

            return nearEnemey;
        }

        private void ReleaseHost()
        {
            if (_hostEnemy != null)
            {
                _hostEnemy.DeathEvent -= Death;
                if (_hostEnemy.IsSlave) _hostEnemy.FreeFromSlavery();
                Venom.SetActive(true);
            }

            _hostHpSlider.gameObject.SetActive(false);
            _host = null;
            _hostEnemy = null;
            _inHost = false;
            Debug.Log("Release Stop");
        }

        // Health
        private void Death()
        {
            Debug.Log("Player Death");
            _hpSlider.value = 0;
            _currentHp = 0;

            gameObject.SetActive(false);
            _hostHpSlider.gameObject.SetActive(false);
            _hpSlider.gameObject.SetActive(false);

            GameSingleton.Instance.Death();
        }

        public void RecieveDamage(float hitPoints)
        {
            if (_inHost)
            {
                _hostEnemy.UpdateHp(_hostEnemy.Health - hitPoints);
            }
            else
            {
                Debug.Log("Receive Damage");
                _currentHp = Mathf.Clamp(_currentHp - hitPoints, 0, _maxHpValue);
            }
        }
    }
}