using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Player
{
    public class VenomController : MonoBehaviour
    {
        private Transform _transform;
        [SerializeField] private Slider hpSlider;
        [SerializeField] private Slider hostHpSlider;
        [SerializeField] public LayerMask enemyLayerMask;
        private Enemy.Enemy _lastHighlightEnemy;

        private void Start()
        {
            hpSlider.gameObject.SetActive(true);
        }

        private void captureEnemy()
        {
        }

        private void FixedUpdate()
        {
            IsEnemyInSphere();
        }

        private void highlightEnemyHp(GameObject enemyObject)
        {
            if (_lastHighlightEnemy == null)
            {
                _lastHighlightEnemy = enemyObject.GetComponent<Enemy.Enemy>();
            }

            hostHpSlider.gameObject.SetActive(true);
            hostHpSlider.value = _lastHighlightEnemy.Health * 0.01f;
        }

        private void IsEnemyInSphere()
        {
            var colliders = Physics.OverlapSphere(transform.position, 2.5f, enemyLayerMask);
            if (colliders.Length > 0)
            {
                highlightEnemyHp(colliders[0].gameObject);
            }
            else
            {
                _lastHighlightEnemy = null;
                hostHpSlider.gameObject.SetActive(false);
            }
        }

        private bool IsEnemyInSight()
        {
            RaycastHit hit;
            var rayDirection = transform.forward * 10f;
            if (Physics.Raycast(transform.position, rayDirection, out hit))
            {
                if (hit.transform.CompareTag("Enemy"))
                {
                    highlightEnemyHp(hit.transform.gameObject);
                    return true;
                }
                else
                {
                }
            }

            _lastHighlightEnemy = null;
            hostHpSlider.gameObject.SetActive(false);
            return false;
        }
    }
}