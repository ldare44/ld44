﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed;
    public Transform ModelTransform;
    
    private CharacterController _characterController;
    private Vector3 _moveDirection = Vector3.zero;
    private Transform _transform;
    
    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
    }

    void Start()
    {
        _transform = transform;
    }

    void Update()
    {
        var input = GetInput();
        
        //Vector3 desiredMove = (_transform.forward * input.y) + (_camera.transform.right * input.x);
        Vector3 desiredMove = new Vector3(
            _transform.position.x * input.x,
            _transform.position.y,
            _transform.position.z * input.y);
        
        //Debug.Log("Move vector = " + desiredMove + "; input = " + input);
        
        UpdateTargetDirection(input.x, input.y);
        UpdateRotation();
        //_moveDirection = 
        //_characterController.Move(desiredMove * Time.deltaTime);
    }
    
    public Vector2 GetInput(string prefix = "", bool debug = false)
    {
        Vector2 input = new Vector2
        {
            x = Input.GetAxis(prefix + "Horizontal"),
            y = Input.GetAxis(prefix + "Vertical")
        };
        if (input.sqrMagnitude > 1) input.Normalize(); // ???? Dafug
        return input;
    }    
    
    public Vector2 GetRawInput(string prefix = "", bool debug = false)
    {
        Vector2 input = new Vector2
        {
            x = Input.GetAxis(prefix + "Horizontal"),
            y = Input.GetAxis(prefix + "Vertical")
        };
        return input;
    }
    
    public void UpdateTargetDirection(float xMagnitude, float zMagnitude)
    {
        // The new movement vector is calculated
        var newMovement = (_transform.forward * -zMagnitude) + _transform.right * xMagnitude;
        // Magnitude fix
        newMovement = Vector3.ClampMagnitude(newMovement,1/Mathf.Sqrt(2)) * Mathf.Sqrt(2);
        //Debug.Log("movement" + newMovement);
        // The player is moved according to his speed
        _characterController.Move(newMovement * Time.deltaTime * MoveSpeed);
           
    }

    private void UpdateRotation()
    {
        var input = GetRawInput("Right");
        //Vector3 lookDirection = new Vector3(Vector3.right * input.x, 0, input.y);
        Vector3 lookDirection = Vector3.right * input.x + Vector3.forward * -input.y;
        if (lookDirection.sqrMagnitude > 0.0f)
        {
            Debug.Log("lookdir = " + lookDirection);
            ModelTransform.localRotation = Quaternion.LookRotation(lookDirection, Vector3.up);
        }

    }
}
