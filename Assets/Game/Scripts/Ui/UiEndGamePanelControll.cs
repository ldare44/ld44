using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Ui
{
    public class UiEndGamePanelControll : MonoBehaviour
    {
        public Text MainText;
        public Text TimerText;
        public Text KillsText;
        
        public void ActivatePanel(float timerCounter = 0, bool lose = false)
        {
            MainText.text = lose ? "Wasted" : "Wou Win";
            
            var timeSpan = TimeSpan.FromSeconds(timerCounter);
            TimerText.text = $"{timeSpan.Minutes}M {timeSpan.Seconds}S";

            KillsText.text = GameSingleton.Instance.KillsCount.ToString();
        }
        
        public void RestartLevel()
        {
            GameSingleton.Instance.RestartLevel();
        }

        public void Credits()
        {
            GameSingleton.Instance.StartCreditsScene();
        }
    }
}