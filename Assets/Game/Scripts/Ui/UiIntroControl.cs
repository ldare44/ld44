﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Ui
{
    public class UiIntroControl : MonoBehaviour
    {
        public Transform tutorialPanel;
        
        void Awake()
        {
            tutorialPanel.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (Input.anyKeyDown && !tutorialPanel.gameObject.activeSelf)
            {
                tutorialPanel.gameObject.SetActive(true);
                return;
            }

            if (Input.anyKeyDown && tutorialPanel.gameObject.activeSelf)
            {
                SceneManager.LoadScene("FinalLevel_v2");
            }
        }
    }
}