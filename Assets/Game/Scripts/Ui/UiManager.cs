using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Scripts.Ui
{
    public class UiManager : MonoBehaviour
    {
        public static UiManager Instance;

        public Transform EndGamePanel;

        private float timerCounter;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            else if (Instance != null)
            {
                Destroy(this);
            }

            InitializeUi();
        }

        private void InitializeUi()
        {
            EndGamePanel.gameObject.SetActive(false);
        }

        private void Update()
        {
            timerCounter += Time.deltaTime;
        }

        public void ActivatePanel(bool lose = false)
        {
            EndGamePanel.gameObject.SetActive(true);
            EndGamePanel.GetComponent<UiEndGamePanelControll>().ActivatePanel(timerCounter, lose);
        }
    }
}