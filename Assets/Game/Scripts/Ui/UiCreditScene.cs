﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Scripts.Ui
{
    public class UiCreditScene : MonoBehaviour
    {
        public Transform AnyKey;

        private bool _anyKeyActive;
        // Update is called once per frame
        private void Awake()
        {
            _anyKeyActive = false;
            AnyKey.transform.gameObject.SetActive(_anyKeyActive);
        }

        public void SetActive()
        {
            _anyKeyActive = true;
            AnyKey.transform.gameObject.SetActive(_anyKeyActive);
        }

        void Update()
        {
            if (_anyKeyActive && Input.anyKeyDown)
            {
                SceneManager.LoadScene("Intro");
            }
        }
    }
}
